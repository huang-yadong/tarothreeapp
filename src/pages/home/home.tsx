import { Component, useState } from 'react'
import { AtButton, AtImagePicker, AtPagination } from 'taro-ui';
import { View, Image, ScrollView } from '@tarojs/components'
import Taro from '@tarojs/taro';

import './home.scss'

type stateType = {
  imageList: string[],
  total: number,
  size: number,
  current: number,
  files: object
};
export default class Home extends Component<any, stateType> {
  constructor(props) {
    super(props);
    this.state = {
      imageList: [],
      total: 0,
      size: 2,
      current: 1,
      files: []
    }
  }

  componentWillMount() { }

  componentDidMount() {
    console.log("xuan然完毕");
    const pageData = {
      current: this.state.current
    }
    this.requestMethod(pageData);
  }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  /**
     * current: this.state.current,
     * size: this.state.size
     */
  requestMethod(pageData) {
    Taro.request({
      url: APPGLOBAL.HOSTWEAPP + '/image/page/images', // index.js 全局变量
      method: 'GET',
      data: {
        current: pageData.current,
        limit: this.state.size
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: (response) => {
        console.log(response.data);
        console.log(this.state);
        const result = response.data;
        let images = new Array();
        for (let item of result.records) {
          images.push(item.imageUrl)
        }
        console.log(result.total);
        this.setState({
          imageList: images,
          current: pageData.current,
          size: result.size,
          total: result.total
        }, () => {
          console.log("aa");
        });
      }
    })
  }

  httpRequest(src) {
    return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest()
      xhr.open('GET', src, true)
      xhr.responseType = 'blob'
      xhr.onload = function (e) {
        if (this.status == 200) {
          let myBlob = this.response
          let files = new window.File(
            [myBlob],
            '2.pdf',
            { type: myBlob.type }
          ) // myBlob.type 自定义文件名
          resolve(files)
        } else {
          reject(false)
        }
      }
      xhr.send()
    })
  }

  blobtofile(src) {
    let myBlob = null;
    Taro.request({
      url: src,
      method: 'GET',
      data: {

      },
      header: {

      },
      success: (response) => {
        console.log(response.data);
        console.log(this.state);
        myBlob = response.data;
      }
    })
    return myBlob
  }
  render() {
    const scrollStyle = {

    }
    return (
      <ScrollView className='components-page'
        scrollY
        scrollWithAnimation
        scrollTop={20}
        style={scrollStyle}
      >
        <View style='heiht: 100%'>
          {
            this.state.imageList.map(
              (e, i) => {
                return (
                  <Image
                    style='width: 40%;height: 150px;background: #fff; margin: 3% 3% 3% 3%'
                    src={e}
                    key={i}
                  />
                )
              }
            )
          }
        </View>
        <AtPagination className='page'
          total={this.state.total}
          pageSize={this.state.size}
          current={this.state.current}
          onPageChange={pageData => {
            this.requestMethod(pageData);
          }
          }
        >
        </AtPagination>

        <View style=' margin: 10%'>
          <AtImagePicker
            multiple
            files={this.state.files}
            onChange={(files) => {
              this.setState({
                files
              })
              console.log(files)
            }}
            onFail={() => {

            }}
            onImageClick={() => {

            }}
          />
          <View style='text-align:center; width: 50%; margin: 10% 0 0 25%'>
            <AtButton style='margin-top: 10%' type='primary' onClick={() => {
              this.state.files.map(
                async (e, i) => {
                  console.log(e);
                  let name = 'tt.jpg'
                  let type = 'jpg'
                  let url = e.url
                  // const imgBlob = await fetch(url).then(r => r.blob())
                  // const imgFile = new File([imgBlob], name, { type: type })
                  // console.log(imgFile)
                  Taro.uploadFile({
                    url: APPGLOBAL.HOSTWEAPP + 'image/upload',
                    // 微信支持吃本地路径字符串, taro ui 是blob url图片,需转化为File 对象,通过http 传输, H5解析的是blob, 
                    // 编译成微信小程序代码后, 是https图片路径. 不需要转化为file对象, 直接上传
                    filePath: url,
                    name: 'files', //后台得的的参数key值
                    fileName: name,
                    formData: {
                      type: type
                    }
                  })
                }
              )
            }}> 上传
            </AtButton>
          </View>
        </View>
      </ScrollView >

    )
  }
}
