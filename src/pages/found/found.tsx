import { Component } from 'react'
import { AtGrid, AtMessage } from 'taro-ui'
import { View, Text } from '@tarojs/components'
import './found.scss'
import Taro from '@tarojs/taro';



export default class Found extends Component {
  constructor(props) {
    super(props);
    let height = window.innerHeight * 0.98;
    let width = window.innerWidth * 0.98;
    this.state = {
      height: height,
      width: width
    }
  }

  componentWillMount() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    return (
      <View style={{ height: this.state.height, width: this.state.width }}>
        <AtMessage className='message' />
        <Text className='text'>模块一</Text>
        <AtGrid className='atgrid' data={
          [
            {
              image: 'https://img12.360buyimg.com/jdphoto/s72x72_jfs/t6160/14/2008729947/2754/7d512a86/595c3aeeNa89ddf71.png',
              value: '领取中心',
              id: 'fetchOne'
            },
            {
              image: 'https://img20.360buyimg.com/jdphoto/s72x72_jfs/t15151/308/1012305375/2300/536ee6ef/5a411466N040a074b.png',
              value: '找折扣',
              id: 'fetchTwo'
            },
            {
              image: 'https://img10.360buyimg.com/jdphoto/s72x72_jfs/t5872/209/5240187906/2872/8fa98cd/595c3b2aN4155b931.png',
              value: '领会员',
              id: 'fetchThree'
            },
            {
              image: 'https://img12.360buyimg.com/jdphoto/s72x72_jfs/t10660/330/203667368/1672/801735d7/59c85643N31e68303.png',
              value: '新品首发',
              id: 'fetchFour'
            },
            {
              image: 'https://img14.360buyimg.com/jdphoto/s72x72_jfs/t17251/336/1311038817/3177/72595a07/5ac44618Na1db7b09.png',
              value: '打豆豆',
              id: 'fetchFive'
            },
            {
              image: 'https://img30.360buyimg.com/jdphoto/s72x72_jfs/t5770/97/5184449507/2423/294d5f95/595c3b4dNbc6bc95d.png',
              value: '手机馆',
              id: 'fetchSix'
            }
          ]
        }
          onClick={(item, index) => {
            console.log("模块一");
            console.log(item);
            console.log(index);
            Taro.atMessage({
              'message': '您点击了模块一: ' + item.value + ";   id: " + item.id,
              'type': 'info'
            })
          }}
        />

        <Text className='text'>模块二</Text>
        <AtGrid className='atgrid' mode='rect' data={
          [
            {
              image: 'https://img12.360buyimg.com/jdphoto/s72x72_jfs/t6160/14/2008729947/2754/7d512a86/595c3aeeNa89ddf71.png',
              value: '领取中心',
              id: 'fetch1'
            },
            {
              image: 'https://img20.360buyimg.com/jdphoto/s72x72_jfs/t15151/308/1012305375/2300/536ee6ef/5a411466N040a074b.png',
              value: '找折扣',
              id: 'fetch2'
            },
            {
              image: 'https://img10.360buyimg.com/jdphoto/s72x72_jfs/t5872/209/5240187906/2872/8fa98cd/595c3b2aN4155b931.png',
              value: '领会员',
              id: 'fetch3'
            },
            {
              image: 'https://img12.360buyimg.com/jdphoto/s72x72_jfs/t10660/330/203667368/1672/801735d7/59c85643N31e68303.png',
              value: '新品首发',
              id: 'fetch4'
            },
            {
              image: 'https://img14.360buyimg.com/jdphoto/s72x72_jfs/t17251/336/1311038817/3177/72595a07/5ac44618Na1db7b09.png',
              value: '打豆豆',
              id: 'fetch5'
            },
            {
              image: 'https://img30.360buyimg.com/jdphoto/s72x72_jfs/t5770/97/5184449507/2423/294d5f95/595c3b4dNbc6bc95d.png',
              value: '手机馆',
              id: 'fetch6'
            }
          ]
        }
          onClick={(item, index) => {
            console.log("模块二");
            console.log(item);
            console.log(index);
            Taro.atMessage({
              'message': '您点击了模块二: ' + item.value + ";  id: " + item.id,
              'type': 'info'
            })

          }}
        />

      </View>
    )
  }
}
