import { Component } from 'react'
import { AtButton } from 'taro-ui'
import Taro from '@tarojs/taro'
import { View } from '@tarojs/components'
import './me.scss'



export default class Me extends Component<any, any>{

  componentWillMount() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  // 微信用户登陆,获取微信账户id
  login(weappUserInfo) {
    //Taro.reLaunch(option)
    // 用户登陆是否过期
    // console.log("未过期");
    // // session_key 已经失效，需要重新执行登录流程
    // //重新登录
    // Taro.login({
    //   success: (res) => {
    //     if (res.code) {
    //       //发起网络请求
    //       const param = {
    //         rowData: weappUserInfo.rowData,
    //         iv: weappUserInfo.iv,
    //         cloudid: weappUserInfo.cloudID,
    //         signature: weappUserInfo.signature,
    //         encryptedData: weappUserInfo.encryptedData,
    //         avatarUrl: weappUserInfo.userInfo.avatarUrl,
    //         gender: weappUserInfo.userInfo.gender,
    //         nickName: weappUserInfo.userInfo.nickName,
    //         code: res.code
    //       };
    //       Taro.request({
    //         url: '',
    //         data: param
    //       })
    //     } else {
    //       console.log('登录失败！' + res.errMsg)
    //     }
    //   },
    //   complete: (res) => {
    //     console.log("登陆接口调用成功");
    //     console.log(res);
    //   }
    // })
  }
  render() {
    return (
      <View style='heiht: 219px'>
        <AtButton type='primary' onClick={() => {
          const accountInfo = Taro.getAccountInfoSync();
          console.log(accountInfo);
          Taro.getUserProfile({
            desc: '登陆黄庄小程序', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
            success: (res) => {
              // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
              console.log(res);
              this.login(res);
              // 将用户信息放在storage中
              Taro.setStorage({
                key: "huangyadongappOpenid",
                data: res
              })
              Taro.switchTab({
                url: '/pages/userinfo/userinfo'
              })
            }
          })
        }}
        >微信一键登陆</AtButton>
      </View>
    )
  }
}
