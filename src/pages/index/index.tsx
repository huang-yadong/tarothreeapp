import { Component } from 'react'
import { View, Video } from '@tarojs/components'
import './index.scss'

export default class Index extends Component {
  // state = {
  //   nodes: [{
  //     name: 'div',
  //     attrs: {
  //       class: 'div_class',
  //       style: 'line-height: 60px; color: red;'
  //     },
  //     children: [{
  //       type: 'text',
  //       text: 'Hello World!'
  //     }]
  //   }]
  // }
  componentWillMount() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  onTabItemTap(Object) {
    console.log("go");
    console.log(Object);
  }
  render() {
    return (
      <View className='bodymyself' >
        <View className='at-article__h1'>
          毛泽东诗词
        </View>
        <View className='at-article__info'>
          2022-02-22&nbsp;&nbsp;&nbsp;毛泽东 &nbsp;&nbsp;&nbsp;阅读量 10
        </View>
        <View className='at-article__content'>
          <View className='at-article__section'>
            <View className='at-article__h2' style='text-align: center;'>
              七律·人民解放军占领南京
            </View>
            <View className='at-article__p' style='text-align: center;  margin-top: 5%'>
              钟山风雨起苍黄，百万雄师过大江。
            </View>
            <View className='at-article__p' style='text-align: center'>
              虎踞龙盘今胜昔，天翻地覆慨而慷。
            </View>
            <View className='at-article__p' style='text-align: center'>
              宜将剩勇追穷寇，不可沽名学霸王。
            </View>
            <View className='at-article__p' style='text-align: center'>
              天若有情天亦老，人间正道是沧桑。
            </View>
          </View>
        </View>
      </View>
    )
  }
}
