import { Component } from 'react'
import Taro from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import { AtAvatar, AtButton, AtIcon, AtList, AtListItem, AtMessage } from 'taro-ui'
import './userinfo.scss'

export default class Userinfo extends Component<any, any> {

  constructor(props) {
    super(props);
    let height = window.innerHeight * 0.98;
    let width = window.innerWidth * 0.98;
    const userInfo = Taro.getStorageSync('huangyadongappOpenid');
    if (userInfo == "" || userInfo == undefined) {
      this.state = {
        avatarUrl: '',
        nickName: '立即登录',
        height: height,
        width: width
      }
    } else {
      this.state = {
        avatarUrl: userInfo.userInfo.avatarUrl,
        nickName: userInfo.userInfo.nickName,
        height: height,
        width: width,
        buttonout: true
      }
    }
    console.log(this.state);
  }


  componentWillMount() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  login(e) {
    console.log(e);
    if (this.state.nickName == '立即登录') {
      Taro.reLaunch({
        url: '/pages/me/me'
      })
    } else {

    }
  }

  loginOut() {
    console.log('退出登录');
    Taro.removeStorageSync('huangyadongappOpenid');
    this.forceUpdate();
    this.setState({
      avatarUrl: '',
      nickName: '立即登录',
      buttonout: false
    }, () => {
      console.log("aa");
    });


  }

  render() {
    // consot va = 
    // if (this.state.buttonout==true)
    // <AtButton className='button' type='primary' size='normal'
    //   onClick={() => { this.loginOut() }
    //   }
    // >退出登录</AtButton>

    return (
      <View style={{ height: this.state.height, width: this.state.width }}>
        <AtMessage />
        <View className='at-row at-row__align--center cloview'>
          <View className='at-col at-col-3' style='display: bolock; padding-left: 24px'>
            <AtAvatar image={this.state.avatarUrl}
              circle
            >
            </AtAvatar>
          </View>

          <View className='at-col at-col-8' style=''>
            <Text onClick={(e) => { this.login(e) }} >{this.state.nickName}</Text>
            <Text style='display: block'>手机号: 123456789 (固定值)</Text>
          </View>

          <View className='at-col at-col-1'>
            <AtIcon value='user' size='30' color='black'></AtIcon>
          </View>

        </View>

        <AtList className='atlist'>
          <AtListItem
            title='个人信息'
            arrow='right'
            thumb='https://img12.360buyimg.com/jdphoto/s72x72_jfs/t6160/14/2008729947/2754/7d512a86/595c3aeeNa89ddf71.png'
            onClick={function () {
              console.log(this);
              Taro.atMessage({
                'message': '敬请期待: 您点击了: ' + this.title,
                'type': 'info'
              })
            }}
          />
          <AtListItem
            title='会员中心'
            note='描述信息: 会员中心'
            arrow='right'
            thumb='https://img10.360buyimg.com/jdphoto/s72x72_jfs/t5872/209/5240187906/2872/8fa98cd/595c3b2aN4155b931.png'
            onClick={function () {
              console.log(this);
              Taro.atMessage({
                'message': '敬请期待: 您点击了: ' + this.title,
                'type': 'info'
              })
            }}
          />
          <AtListItem
            title='服务中心'
            note='描述信息: 服务中心'
            extraText='详细信息'
            arrow='right'
            thumb='https://img12.360buyimg.com/jdphoto/s72x72_jfs/t10660/330/203667368/1672/801735d7/59c85643N31e68303.png'
            onClick={function () {
              console.log(this);
              Taro.atMessage({
                'message': '敬请期待: 您点击了: ' + this.title,
                'type': 'info'
              })
            }}
          />
        </AtList>
        {this.state.buttonout ? <AtButton className='button' type='primary' size='normal'
          onClick={() => { this.loginOut() }
          }
        >退出登录</AtButton> : null}

      </View>
    )
  }
}
