export default {
  pages: [
    'pages/home/home',
    'pages/index/index',
    'pages/userinfo/userinfo',
    'pages/me/me',
    'pages/found/found',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'huangzhuang',
    navigationBarTextStyle: 'black'
  },
  tabBar: {
    position: 'bottom',
    list: [{ pagePath: 'pages/home/home', text: '壁纸' },
    //{pagePath:'pages/index/index', text: '阅读'},
    { pagePath: 'pages/index/index', text: '阅读' },
    { pagePath: 'pages/found/found', text: '发现' },
    { pagePath: 'pages/userinfo/userinfo', text: '我的' }]
  }
}
