module.exports = {
  env: {
    NODE_ENV: '"production"'
  },
  defineConstants: {
    APPGLOBAL: {
      HOSTWEAPP: '"https://www.huangyadong.top:8443/"'
      //HOSTWEAPP: '"https://localhost:8443/"'
      //HOSTWEAPP: '"https://47.102.142.108:8443/"'
      //HOSTWEAPP: '"https://www.baidu.com/"'
      //HOSTWEAPP: '"https://www.huangyadong.top:8443/"'
    }
  },
  mini: {
    // 微信小程序需要加这个要不然报错
    webpackChain(chain) {
      chain.optimization.sideEffects(false);
      // 解析多端文件 插件
      chain.resolve.plugin('MultiPlatformPlugin')
        .tap(args => {
          args[2]["include"] = ['@taro-mobile']
          return args
        })
    }
  },
  h5: {
    /**
     * 如果h5端编译后体积过大，可以使用webpack-bundle-analyzer插件对打包体积进行分析。
     * 参考代码如下：
     * webpackChain (chain) {
     *   chain.plugin('analyzer')
     *     .use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin, [])
     * }
     */
  },
  // 解析多端文件 插件 假设我们需要解析 NPM 包 taro-mobile 里面的多端文件，可以利用 WebpackChain 为 MultiPlatformPlugin 插件添加 include 配置：
  rn: {
    resolve: {
      include: ['taro-mobile'],
    }
  }
}
